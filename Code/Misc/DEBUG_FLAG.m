function y = DEBUG_FLAG(val_flag)

persistent DB_FLAG;

if isempty(DB_FLAG)
    DB_FLAG = 1; % Default
end
if nargin==0
    y = DB_FLAG;
else
    DB_FLAG = val_flag;
end
