function y = norminv(epsi)
% function y = norminv(epsi)
% Inverse cdf of normal r.v. 
% Compute y s.t. Phi(y) = epsi

y = sqrt(2)*erfinv(2*epsi-1);
