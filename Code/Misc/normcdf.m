function y = normcdf(x,mu,sigma)
% function y = normcdf(x,mu,sigma)
% Prob(X<x) when X~N(mu,sigma^2)
%
% Inputs
% * x : integral from -inf to x
% * mu : mean of the gaussian random variable
% * sigma: std of the gaussian random variable
%
% Outputs
% * y : cdf of X

error(nargchk(1, 3, nargin))

if nargin==1
    % CDF of a gaussian variable N(0,1)
    mu = 0;
    sigma = 1;
end
if nargin ==2
    % CDF of a gaussian variable N(mu,1)
    sigma = 1;
end

if sigma==0
    y = 0;
    if x>=mu
        y=1;
    end
else
    %y = (1+erf((x-mu)/sqrt(2)/sigma))/2;
    y = erfc(-(x-mu)/sqrt(2)/sigma)/2;
end
