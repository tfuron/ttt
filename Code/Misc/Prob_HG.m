function y =Prob_HG(n1,n2,psi,m1)
% function y =Prob_HG(n1,n2,psi,m1)
%
% Distribution of a Noncentral Hypergeometric r.v.
% see "Fast and stable algorithms for computing and sampling from the noncentral Hypergeometric Distribution"
% Liao & Olsen
% The American Statistician, Vol. 55, No. 4
%
% m1 must be an integer
% n1, n2, psi, can be vectors of the same length  [m x 1]

m = length(n1);

y = zeros(m,m1+1);
% Compute the mode
eta = mode_HG(n1,n2,psi,m1);
y(:,eta+1)=1; % Indices start from 1 in Matlab
% Compute the upper and lower limits
l = max(0,m1-n2);
u = min(n1,m1);

for km=1:m
    for k=(eta(km)+1):u(km)
        r = (n1(km)-k+1)*(m1-k+1)/k/(n2(km)-m1+k)*psi(km);
        y(km,k+1)=y(km,k)*r; % Indices start from 1 in Matlab
    end
    for k=(eta(km)-1):-1:l(km)
        ii = k+1;
        r = (n1(km)-ii+1)*(m1-ii+1)/ii/(n2(km)-m1+ii)*psi(km);
        y(km,k+1)=y(km,k+2)/r;
    end
    y(km,:) = y(km,:)/sum(y(km,:));
end