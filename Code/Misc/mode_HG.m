function y = mode_HG(n1,n2,psi,m1)
% function y = mode_HG(n1,n2,psi,m1)
% Compute the mode of a HyperGeometric distribution
%
% "Fast and stable algorithms for computing and smapling from the noncentral
% hypergezometric distribution"
% Liao and Rosen
% The american statistician, Vol. 55, No. 4 (Nov. 2001)
% See Appendix of this article
%
% Inputs
% n1, n2, psi, m1 are either scalar either vector of same length

% Here psi = 1, therefore a = 0 and root = -c/b

l = max(0,m1-n2);
u = min(n1,m1);

% Compute solution of the equation
A = psi-1;
B = -((n1+m1+2).*psi+n2-m1);
C = psi.*(n1+1).*(m1+1);
Q = -(B + sign(B).*sqrt(B.^2-4*A.*C))/2;
y = C./Q;

% Verification
y(y<l)=l(y<l);
y(y>u)=u(y>u);
y = floor(y);
