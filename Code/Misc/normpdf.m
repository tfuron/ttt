function y = normpdf(x,mu,sigma)
% function y = normpdf(x,mu,sigma)
% f_X(x) when X~N(mu,sigma^2)
%
% Inputs
% * x : PDF in x
% * mu : mean of the gaussian random variable
% * sigma: std of the gaussian random variable
%
% Outputs
% * y : PDF of X in x

error(nargchk(1, 3, nargin))

if nargin==1
    % PDF of a gaussian variable N(0,1)
    mu = 0;
    sigma = 1;
end
if nargin ==2
    % CDF of a gaussian variable N(mu,1)
    sigma = 1;
end

if sigma==0
    y = zeros(size(x));
    y(x==mu) = 1;
else
    y = exp(-(x-mu).^2/2/sigma^2)/sqrt(2*pi)/sigma;
end