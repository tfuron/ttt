function y = code_pack(x)
% function y = code_pack(x)
%
% Pack a binary code matrix m x n
% into an interger code matrix m/8 x n
% each symbol ranging in [0, 1, ..., 255]

[m, n] = size(x);
if mod(m,8)~=0
  error('Packing implies that m is a multiple of 8');
end
mp = m/8;
y = zeros(mp,n,'uint8');

W = single(2.^(7:-1:0));

for k=1:mp
  y(k,:) = W*single(x((k-1)*8+(1:8),:));
end