function y = modify_tardos(x,mu,p)
% function y = modify_tardos(x,mu,p)
%
% Modify Tardos sequences (binary or Q-ary)
%
% INPUTS
% .x: binary Tardos sequences    [m x n]
% .mu: replication strength      [1 x 1]
% .p: Tardos secret sequence     [m x 1]
%
% COMMENT
% . This routine is used for the rare event analysis

[m n] = size(x);
if length(p)~=m
    error('Tardos sequences and secret of different lengths!');
end

k = ceil(mu*m); % Number of symbols to be re-drawn
perm = randperm(m);
Ind = perm(1:k); % The symbols are randomly selected

y = x;
if size(p,2)==1 % Binary alphabet
    y(Ind,:)=generate_sequence(p(Ind),n);
else % Q-ary alphabet
    y(Ind,:)=generate_sequence_q(p(Ind,:),n);
end
