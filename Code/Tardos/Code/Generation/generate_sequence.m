function x=generate_sequence(p,n,flag_pack)
%function x=generate_sequence(p,n,flag_pack)
%
% Generates Tardos code sequences
%
% INPUTS
% .p : vector of secret biases P(X_i==1)=p(i) [m x 1]
% .n : n sequences to be generated
%
% OPTIONS
% .flag_pack: pack the codeword by pack of 8 symbols
%            for efficient storage [FALSE]
%
% OUTPUTS
% .x : n sequences [m x n] or [m/8 x n] if flag_pack=true
%      format uint8
%
% COMMENTS
% packing is done such that dec2bin is the reverse operator


[m, cp] = size(p);
n_chunk = 10^4;

%% Check the inputs
if cp~=1
  error('p should be a vector m x 1');
end
if nargin<=2
  flag_pack = false;
end
if flag_pack
  if mod(m,8)~=0
    error('Packing implies that m is a multiple of 8');
  end
  mp = m/8;
  x = zeros(mp,n,'uint8');
else
  x = zeros(m,n,'uint8');
end

%% Generate by batch of nchunk sequences
if n<=n_chunk
  p_mat = repmat(p,1,n);
  x_tmp = uint8((rand(m,n)<p_mat));
  if flag_pack
    x = code_pack(x_tmp);
  else
    x = x_tmp;
  end
else
  p_mat = repmat(p,1,n_chunk);
  x_tmp =  zeros(m,n_chunk,'uint8');
  K = fix(n/n_chunk);
  for k=1:K
    x_tmp = uint8((rand(m,n_chunk)<p_mat));
    z = ((k-1)*n_chunk+1):(k*n_chunk);
    if flag_pack
      x(:,z) = code_pack(x_tmp);
    else
      x(:,z) = x_tmp;
    end
  end
  if z(end)<n
    z = (K*n_chunk+1):n;
    p_mat = repmat(p,1,n-K*n_chunk);
    x_tmp = uint8((rand(m,n-K*n_chunk)<p_mat));
    if flag_pack
      x(:,z) = code_pack(x_tmp);
    else
      x(:,z) = x_tmp;
    end
  end
end
