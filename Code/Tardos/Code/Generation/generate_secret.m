function pb = generate_secret(m,varargin)
%function pb=generate_secret(m,varargin)
% Initializes the Tardos code by creating the secret bias sequence p
%
% Inputs
% .m : length of the code
% .options: a structure containing the following parameters
%
% Parameters:
%   .verbose: list the inputs               [true]
%   .t: cutoff parameter                    [1/300/3]
%   .method: type of generator              ['tardos']
%       'tardos','optimal_single', or 'optimal_joint'
%   .c: number of expected colluders        [5]
%
% Outputs
% .pb: the secret bias sequence

%% Parsing the arguments
% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('m',@(x)validateattributes(x,{'numeric'},{'integer','positive'}));

% Define optional inputs
p.addParamValue('verbose',false,@(x)islogical(x));
p.addParamValue('t',1/300/3,@(x)isnumeric(x) && isscalar(x) && x>=0 && x<0.5);
p.addParamValue('method','tardos',@(x)strcmpi(x,'tardos')||strcmpi(x,'optimal_single')||strcmpi(x,'optimal_joint'));
p.addParamValue('c',5,@(x)validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive', '<=', 20}));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(m,varargin{:});

h = p.Results;

%% Display the names of all arguments.
if h.verbose
    fprintf('\n')
    disp 'List of all arguments of generate_secret:'
    disp(p.Results)
end

%% Generate
switch(h.method)
    case 'tardos'
        t_prime = asin(sqrt(h.t));
        r = t_prime + (pi/2-2*t_prime)*rand(h.m,1);
        pb = sin(r).^2;
    case 'optimal_single'
        [pp,w] = f_saddlepoint_single(c);
        ind = zeros(m,1);
        for ki=1:m
            ind(ki) = generate_discrete(w);
        end
        pb = pp(ind);
        
    case 'optimal_joint'
        [pp,w] = load_joint(h.c);
        ind = zeros(m,1);
        for ki=1:m
            ind(ki) = generate_discrete(w);
        end
        pb = pp(ind);
    case 'laarhoven' 
        [pp,w] = f_laarhoven(c);
        ind = zeros(m,1);
        for ki=1:m
            ind(ki) = generate_discrete(w);
        end
        pb = pp(ind);
        
    case 'nuida'
        error('Not implemented yet');
        
end

%% Internal functions
function y = generate_discrete(w)
% Generate discrete r.v. according to distribution w
a = rand;
k = 1;
while (a>0)
    a = a-w(k);
    k = k+1;
end
y = k-1;