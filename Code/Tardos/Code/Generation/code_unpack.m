function y = code_unpack(x)
% function y = code_unpack(x)
%
% Unpack an integer code matrix m/8 x n
% (each symbol being in [0, 1, ..., 255])
% into a binary code matrix m x n


[mp, n] = size(x);
y = zeros(8*mp,n,'uint8');

W = uint8(2.^(7:-1:0));
ind = (0:(mp-1))*8;

for k=1:8
  z = k+ind;
  y(z,:) = mod(idivide(x,W(k)),2);
end