function [th_out,correct_flag] = theta_check(th_in)
% Make sure theta is a collusion model
% by clipping probabilities in [0,1]
% and normalizing to sum up to one
%
% Input
% .th_in: collusion model [Q x (c+1)]
%
% Output
% .th_out: collusion model [Q x (c+1)]

th_out = th_in;

% Clipped theta
th_out(th_in<0)=0;
th_out(th_in>1)=1;

% Normalized
th_out = bsxfun(@times,th_out,1./sum(th_out,1));

% Check
if norm(th_out-th_in)>10^-5
    correct_flag = false;
else
    correct_flag = true;
end

