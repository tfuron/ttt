function y = collude(X,varargin)
%function y = collude(X,options)
%
% Collusion of several colluders' sequences
%
% INPUTS
% .X: colluders's sequences [m x c]
% .option: a structure containing the following parameters
%
% OUTPUT
% .y: the pirated sequences [m x 1] / Format 'uint8'
%
% PARAMETERS
%   .strategy: collusion strategy           ['Uniform']
%    ('Minority', 'Majority','CoinFlip', 'Random','Uniform','Assign'...
%       ... 'All1', 'All0')
%   .assignation: assignation sequence = who is pasting which symbol
%   .verbose: give the inputs
%   .model: a [Q x c+1] matrix of Prob[Y=y|S]
%   (set 'strategy' to 'Random' to activate this collusion) 
%
% EXAMPLES
% y=collude(X)                     Collusion is 'Uniform'
% y=collude(X,'strategy','Random') Collusion is 'Random' with
%                                  Prob[Y=1|S]=S/c - ie. Uniform
% y=collude(X,'strategy','Random','model',theta)
%                                  Collusion is 'Random' with
%                                  Prob[Y=1|S] given by theta
%
% COMMENTS
% .This uses Matlab parsing mechanism. Options can be passed in a structure
% or inline

%% Parsing the arguments

% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('X',@(x)validateattributes(x,{'numeric'},{'2d'}));

[m c]=size(X);
assign_default = repmat(1:c,1,ceil(m/c));

% Define optional inputs
Strat_array = {'majority','minority','uniform','coinFlip','random','assign','all1','all0'};
p.addParamValue('strategy','Uniform',@(x)any(strcmp(lower(x),Strat_array)));
p.addParamValue('verbose',false,@(x)islogical(x));
p.addParamValue('assignation',assign_default(1:m),@(x)validateattributes(x,{'numeric'},{'size',[1,m]}));
p.addParamValue('model',[0:c;c:-1:0]/c,@(x)(isnumeric(x)&&isequal(size(x,2),c+1)));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(X,varargin{:});

h = p.Results;

% Display the names of all arguments.
if h.verbose
    fprintf('\n')
    disp 'List of all arguments of collude:'
    disp(p.Results)
end

%% The big switch
y = zeros(m,1);
switch lower(h.strategy)
    case 'assign' % collude according to an assignation sequence
        l = 1:m;
        y = h.X(l' + (h.assignation(l)-1)*m);
        
    case 'uniform' % draw uniformly which colluder pastes the next symbol
        h.assignation = randi(c,m,1);
        l = 1:m;
        y = h.X(l' + (h.assignation(l)-1)*m);
        
    case 'majority' % Majority vote
        y = round(sum(h.X,2)/c);
        if mod(c,2)==0 % flip a coin when as many 1 as many 0
            idx = find(sum(h.X,2)==c/2);
            y(idx) = randi(2,length(idx),1)-1;
        end
        
    case 'minority' % Minority vote
        s = sum(h.X,2)/c;
        y = round(1-s);
        y(s==0)=0;
        y(s==1)=1;
        if mod(c,2)==0 % flip a coin when as many 1 as many 0
            idx = find(sum(h.X,2)==c/2);
            y(idx) = randi(2,length(idx),1)-1;
        end
        
    case 'coinflip' % flip a coin to paste '1' or '0'
        s = sum(h.X,2);
        y = randi(2,m,1)-1;
        y(s==0)=0;
        y(s==c)=1;
        
    case 'random' % collude according to a strategy model
        s = sum(h.X,2);
        theta = h.model(:,s+1);
        a = rand(m,1);
        for j=1:m
            tmp = a(j);
            ind = 0;
            while tmp>0
                ind = ind + 1;
                tmp = tmp - theta(ind,j);
            end
            y(j) = ind-1;
        end
               
    case 'all0'
        y = zeros(m,1);
        s = sum(h.X,2);
        y(s==c)=1;
        
    case 'all1'
        y = ones(m,1);
        s = sum(h.X,2);
        y(s==0)=0;
        
    otherwise
        error('Collusion strategy unknown');
end
y = uint8(y);
