function theta = collusion_model_I(c,collusion,opt)
%function theta = collusion_model_I(c,collusion,opt)
%
% Give the collusion model matrix theta
% for a given collusion process of the first kind and its size.
%
% The first kind is defined as:
% . The colluders first paste one of their symbols according to a strategy
% . Then they add erasures (mimicking a video compression for instance)
%
% Inputs:
% .c: number of colluders
% .collusion: 'Minority', 'Majority', 'All1', 'All0', 'Uniform',
%               'WCA_Simple', 'WCA_Joint', 'CoinFlip'
% .opt: option structure
%       - parameters for 'WCA_Simple'
%       - parameters for adding erasures .erasure_rate   
%
% Outputs:
% .theta:   [2 x c+1] fist line: Prob(y=0|s), second line: Prob(y=1|s)
%               if no erasure is required
%           [4 x c+1] if erasures. y lies in {'0','1','2','3'}
%               '2' means double detection
%               '3' means erasures

theta_one = zeros(1,c+1);
switch lower(collusion)
    case 'minority'
        theta_tmp = 1-collusion_model_I(c,'Majority');
        theta_one = theta_tmp(2,:);
        theta = theta_ext(theta_one);
    case 'majority'
        if mod(c,2)==0
            Ix = (c/2+1:c)+1;
            theta_one(c/2+1)=1/2;
        else
            Ix = ((c+1)/2:c)+1;
        end
        theta_one(Ix) = 1;
        theta = theta_ext(theta_one);
    case 'all1'
        theta_one = ones(1,c+1);
        theta = theta_ext(theta_one);
    case 'all0'
        theta_one = zeros(1,c+1);
        theta = theta_ext(theta_one);
    case 'uniform'
        theta_one = (0:c)/c;
        theta = theta_ext(theta_one);
    case 'coinflip'
        theta_one = ones(1,c+1)/2;
        theta = theta_ext(theta_one);
    case 'flip_coin'
        theta_one = ones(1,c+1)/2;
        theta = theta_ext(theta_one);
    case 'wca_simple'
        %error('Not yet validated')
        theta = Find_Worst(c,opt);
    case 'wca_joint'
        error('Not implemented yet')
    otherwise
        error('Do not know this collusion')
end

if exist('opt', 'var')
   if isfield(opt,'erasure_rate')
      theta = [theta; zeros(2,c+1)];
      theta(4,:) = opt.erasure_rate*ones(1,c+1);
      theta(1:2,:) = (1-opt.erasure_rate)*theta(1:2,:);
   end
end

end

function y = theta_ext(theta_one)
    y = [1-theta_one;theta_one];
    y(:,1) = [1; 0];
    y(:,end) = [0; 1]; 
end

