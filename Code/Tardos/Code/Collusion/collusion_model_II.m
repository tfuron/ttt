function theta = collusion_model_II(c,collusion,opt)
%function theta = collusion_model_II(c,collusion,opt)
%
% Give the collusion model matrix theta
% for a given collusion process of the second kind and its size.
%
% The second kind is defined as:
% . The colluders mix their block altogether producing double detection and
% erasures
%
% Inputs:
% .c: number of colluders
% .collusion: 'mixall' , 'mixtwo'
%               
% .opt: option structure
%       - parameters for 'mixall'
%            opt.ratio_deviation: we put a double when s/c is around 1/2+/-ratio_deviation
%            opt.erasure_rate: [1 x 2] rates for pure block and half mixed
%            blocks
%
%       - parameters for 'mixtwo'    
%            opt.double_rate: rate of double
%            opt.erasure_rate: [1 x 2] rates for pure and half mixed blocks
%
% Outputs:
% .theta:   
%           [4 x c+1] y lies in {'0','1','D','X'}
%               '3' means double 'D'  = double detection
%               '4' means 'X' = erasure

%% Check parameters
if nnz(opt.erasure_rate<0)+nnz(opt.erasure_rate>1)>0
            error('Erasure rates must range in [0,1]');
end
if (opt.double_rate<0)||(opt.double_rate>1)
            error('Erasure rates must range in [0,1]');
end
theta = zeros(4,c+1);

%% the big switch
switch lower(collusion)
    case 'mixall'
        % Colluders mix all their blocks producing
        % double detection such that
        % Pr(y=D|s) = Pr(y=D|c - s)
        % Pr(y=D|0) = Pr(y=D|c) = 0
        % and also binary symbols such that
        % Pr(y=0|s) = Pr(y=1|c-s)
        
        s = 0:c;
        ratio = s./c;
        ind  = (abs((ratio-1/2)) < opt.ratio_deviation);
        theta(3,ind) = 1;
        theta(1,(~ind)&(s<c/2)) = theta(1,(~ind)&(s<c/2)) + 1;
        theta(2,(~ind)&(s>c/2)) = theta(2,(~ind)&(s>c/2)) + 1;
        
        % then they add erasures such that
        % Pr(y=X|s) = Pr(y=X|c - s)
        % erasure rates are defined by:
        %  - the rate for non-mixed blocks opt.erasure_rate(1)
        %  - the rate for half mixed blocks opt.erasure_rate(2)
        
        rate = opt.erasure_rate;
        mid = ceil((c+1)/2);
        theta(4,:) = interp1([0 1/2],opt.erasure_rate,1/2-abs(1/2-(0:c)/c));
        theta(1:3,:) = bsxfun(@times,theta(1:3,:),1-theta(4,:));
        
        
    case 'mixtwo'
        % Whenever they can, colluders mix one '0' and one '1' blocks
        % This produces '0', '1' and 'D' such that
        % Pr(y=0|s) = Pr(y=1|s) = (1 - Pr(y=D|s))/2     0<s<c
        % Pr(y=1|0) = Pr(y=0|1) = Pr(y=D|0) = Pr(y=1|D) = 0
        
        theta(3,2:c) = opt.double_rate;
        theta(1:2,2:c) = (1-opt.double_rate)/2;
        theta(1,1) = 1;
        theta(2,c+1) = 1;
        
        % Then they add erasures at random according to 2 rates
        % opt.erasure_rate(1) = rate when s=0 or s=c
        % opt.erasure_rate(2) = rate when mixed block
        
        theta(:,1) = theta(:,1)*(1- opt.erasure_rate(1));
        theta(:,c+1) = theta(:,c+1)*(1- opt.erasure_rate(1));
        theta(4,1) = opt.erasure_rate(1);
        theta(4,c+1) = opt.erasure_rate(1);
        
        theta(:,2:c) = theta(:,2:c)*(1- opt.erasure_rate(2));
        theta(4,2:c) = theta(4,2:c) + opt.erasure_rate(2);
        
    otherwise
        error('Do not know this collusion')
end