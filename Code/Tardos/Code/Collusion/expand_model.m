function theta_out = expand_model(theta_in,K)
%function theta_out = expand_model(theta_in,K)
%
% Compute the equivalent collusion model of size c+K
% of a collusion model of size c
%
% Inputs:
% .theta_in: collusion model     [Q x c+1]
% .K: Number of time the Leave One Out tricks is applied
%
% Outputs:
% .theta_out: the equivalent collusion model

Q = size(theta_in,1);
c_old = size(theta_in,2)-1;
theta_old = theta_in;
for k=1:K
    c_new = c_old+1;
    theta_new = zeros(Q,c_new+1);
    kc = (2:c_new);
    s = repmat(kc-1,Q,1);
    theta_new(:,kc) = (s.*theta_old(:,kc-1)+(c_new-s).*theta_old(:,kc))/c_new;
    theta_new(:,1) = theta_old(:,1);
    theta_new(:,end) = theta_old(:,end);
    theta_old = theta_new;
    c_old = c_new;
end
theta_out = theta_new;