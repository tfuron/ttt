function P = Bound_Bernstein(x,opt)
%function P = Bound_Bernstein(x,opt)
%
% This function computes the Bernstein bound on the probability that
% an innocent gets a score as large as x.
% ***Operational Mode***
%
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Upper bounds of the probability


EU = opt.W(1,:).*(1-opt.p') + opt.W(2,:).*opt.p';
W = opt.W-ones(2,1)*EU;

a = max(abs(W(:)));

EU2 = W.^2*[(1-opt.p), opt.p];
v = EU2(1,1)+EU2(2,2);

z = x - sum(EU);
z(z<0) = 0;

P = exp(-z.^2./(v+a*z/3)/2);

end

