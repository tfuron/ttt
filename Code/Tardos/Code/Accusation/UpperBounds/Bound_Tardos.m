function P = Bound_Tardos(x,opt)
%function P = Bound_Tardos(x,opt)
%
% This function computes the Tardos bound on the probability that
% an innocent gets a score as large as x.
% ***Operational Mode***
%
% This is bound Tardos used in his original paper.
% It works with whatever scoring function
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Upper bounds of the probability


% Operational mode
EU = opt.W(1,:).*(1-opt.p') + opt.W(2,:).*opt.p';
W = opt.W-ones(2,1)*EU; % centering
x = x - sum(EU);
g1_max = max(W(:));
EU2 = W(1,:).^2.*(1-opt.p') + W(2,:).^2.*opt.p';
m = sum(EU2);

tmp = x/2/m;
tmp(tmp<0) = 0;
alpha_max = 1.7/g1_max;

alpha = alpha_max*ones(size(tmp));
ind = (tmp<alpha_max);
alpha(ind) = tmp(ind);

P = exp(alpha.*(m*alpha-x));

end

