function [P,Pmax]  = Estimate_RareEvent(s,opt)
%function [P,Pmax] = Estimate_RareEvent(s,opt)
%
% Estimate the probability that an innocent has a score as high as x
%
% Comments: based on rare event simulation
% .you must have our rare event toolbox
% .available in the current matlab path 
%
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Estimation of the probability
% .Pmax: Upper bound of the 95% confidence interval


H0_setup.mu = 0.05;
H0_setup.t = 10;
H0_setup.n = 200;
H0_setup.cell = false;
H0_setup.verbose = false;
H0_setup.GENERATE = @(dim,x)generate_sequence(opt.p,x);
H0_setup.SCORE = @(x)accusation_score(x,opt);
H0_setup.MODIFY = @(x,mu)modify_tardos(x,mu,opt.p);


[P, Stat] = estimator_B(s,length(opt.p),H0_setup);
tmp = [Stat.interval];
Pmax = tmp(2,:);

