function P = Bound_CramerChernoff(x,opt)
%function P = Bound_CramerChernoff(x,opt)
%
% This function computes the Cramer Chernoff bound on the probability that
% an innocent gets a score as large as x.
% ***Operational Mode***
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Upper bound of the probability

P = zeros(length(x),1);
mk = opt.W(1,:).*(1-opt.p') + opt.W(2,:).*opt.p';
Delta = opt.W(2,:)-opt.W(1,:);

l_max = log(realmax)/min(Delta(Delta>0));

xx = x;

for kx=1:length(x)
    if xx(kx)<=0
        P(kx) = 1;
    else
        fun = @(l)-psi_Bernoulli(l,x(kx),Delta,opt.p');
        [~,f_min] = fminbnd(fun,0,l_max);
        P(kx) = exp(f_min);
    end
end
end

% Subfunction
function y=psi_Bernoulli(lambda,t,Delta,p)

y = lambda*(t+sum(Delta.*p));

for k=1:length(Delta)
    if Delta(k)>0
        y = y - lambda*Delta(k) - log(p(k)+(1-p(k))*exp(-lambda*Delta(k)));
    else
        y = y - log(1-p(k)+p(k)*exp(lambda*Delta(k)));
    end
end
end

