function P = Bound_KearnsSaul(x,opt)
%
% This function computes the Kearns & Saul bound on the probability that
% an innocent gets a score as large as x.
% ***Operational Mode***
%
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Upper bounds of the probability
mk = opt.W(1,:).*(1-opt.p') + opt.W(2,:).*opt.p';


betak = (opt.W(1,:)-opt.W(2,:)).^2;
betak = betak.*(1-2*opt.p')./(log(1-opt.p')-log(opt.p'));

B = sum(betak);

z = x - sum(mk);
z(z<0) = 0;
P = exp(-z.^2/B);

end

