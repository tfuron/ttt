function P = Bound_Hoeffding(x,opt)
%function P = Bound_Hoeffding(x,opt)
%
% This function computes the (imporved) Hoeffding bound on the probability that
% an innocent gets a score as large as x.
% ***Operational Mode***
%
% This is bound Tardos used in his original paper.
% It works with whatever scoring function
%
% Inputs
% .x: vectors of scores [n x 1]
% .opt: structure describing the decoder
%       mandatory fields: opt.p, opt.W
%
% Outputs
% .P: Upper bounds of the probability

mk = opt.W(1,:).*(1-opt.p') + opt.W(2,:).*opt.p';
W = opt.W-ones(2,1)*mk;

ak = min(W);
bk = max(W);

ck = max([-ak;bk]);
vk = W(1,:).^2.*(1-opt.p') + W(2,:).^2.*opt.p';
V = sum(vk);


%B = (6*V+sum((ck-vk./ck).^2))/3; % Eq. (2.73)

D = sum((bk-ak).^2);
B = (2*D/3 + 4*V/3)/2; % Eq.(2.15)

z = x - sum(mk);
P = exp(-z.^2/B);

end
