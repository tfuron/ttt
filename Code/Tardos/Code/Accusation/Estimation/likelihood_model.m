function [val,grad]=likelihood_model(theta,y,p,P_S_P)
% function [val,grad]=likelihood_model(theta,y,p,P_S_P)
%
% Inputs
% .theta: collusion model [Q x (c+1)]
% .y: pirates sequence
% .p: secret biases sequence
%
% Options
% .P_S_P: proba that the colluders have S ones over c [(c+1) x m]
%  typically P_S_P = ProbS_coll(p,c,coll) with
%  coll: the sequences of known colluders
%
% Default :
% .P_S_P = ProbS_coll(p,c)
%
% WARNING:
% for fast computation, this creates a global variable P_S_P
% To be cleared by a call without any argument.

% global P_S_P
% %% Clean
% if nargin==0 % Clear persistent variable
%     P_S_P = [];
%     if (DEBUG_FLAG>=3)
%         disp('[Likelihood_model]: clearing global variables');
%     end
%     return
% end


%% Initialize
[Q,c] = size(theta);
c = c -1;
if ~exist('P_S_P','var')
    P_S_P = ProbS_coll(p,c)';
else
    %P_S_P = P_S_P';
end

%% Compute Likelihood
PY_mat = theta(y+1,:).*P_S_P;
PY_vec = sum(PY_mat,2);
val = sum(log(PY_vec));

%% Compute gradient
% Not validated!
if nargout==2
    grad = zeros(Q,c+1);
    tmp = P_S_P./repmat(PY_vec,1,c+1);
    for kq=1:Q
        ind = find(y==kq-1);
        grad(kq,:) = sum(tmp(ind,:));
    end
end
