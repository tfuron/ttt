function [model, likelihood] = estimate_model_hard_EM(c,y,p,symm,coll)
%function [model, likelihood] = estimate_model_hard_EM(c,y,p,symm,coll)
%
% Estimate the collusion process
% by the EM algorithm
%
% Inputs:
% .c: number of colluders (predicted)
% .y: pirated sequence  [1 x m] in {0,1} or {0,1,2} or {0,1,2,3}
% .p: secret biases     [1 x m]
%
% Options
% .symm: boolean. Symmetric collusion model only (multimedia setup) [true]
% .coll: L known colluders sequences [m x L] (empty by default)
%
% Warning: The rules for y is the following:
% .y = 0: symbol '0'
% .y = 1: symbol '1'
% .y = 2: symbols '0' and '1' detected, a.k.a. double detection
% .y = 3: no symobol detected, a.k.a. erasure
%
% Rules for the collusion are:
% .P(y=0|s=c) = P(y=1|s=0) = 0
% .P(y=2|s=0) = P(y=2|s=c) = 0
%
% More rules for the symmetric collusion are:
% .P(y=0|s) = P(y=1|c-s)
% .P(y=2|s) = P(y=2|c-s) for 0<s<c
% .P(y=3|s) = P(y=3|c-s) for 0<=s<=c
%
% Outputs:
% .model: vector representing the collusion attack [Q x c+1]
% .likelihood

if ~exist('symm','var')
    symm = true;
end
if ~exist('coll','var')
    coll = [];
end
if min(y)~=0
    error('sequence y does not contain any 0 symbol?!?');
end

Q = single(max(y)+1); % alphabet size {0,1,...,Q-1}
model = zeros(Q,c+1);
P_S_P = ProbS_coll(p,c,coll)';
m = length(p);
Ntry = 15;
kmax = 70;
DL_eps = 10^-5;
likelihood = -Inf;
T_k = zeros(m,c+1);

% a simple case
if (Q==2)&&(symm)&&(c==2)
   model(:,2)=1/2;
   model(1,1)=1;
   model(2,3)=1;
   likelihood = likelihood_model(model,y,p,P_S_P);
   return
end

% Computing some priors
prior = ones(Q,1)/Q;
if Q>2
    for kq=1:Q
        prior(kq) = nnz(y==kq-1)/m;
    end
else
    prior = ones(2,1)/2;
end



for ktry = 1:Ntry % We run Ntry E-M algorithms
    %% Initilization
    tau = zeros(Q,c+1);
    
    if ktry==1 % For the first try, we test the uniform distribution
        prototype = collusion_model_I(c,'uniform');
    elseif ktry==2; % For the second, try we test the coinflip
        prototype = collusion_model_I(c,'coinflip');
    else % Otherwise, it is a random guess for symbol 0 and 1
        if symm
            if mod(c,2) % c odd
                tmp2 = rand(1,(c-1)/2);
                tmp = [1, tmp2, 1-fliplr(tmp2), 0];
            else
                tmp2 = rand(1,c/2 - 1);
                tmp = [1,tmp2,1/2,1-fliplr(tmp2),0];
            end
        else
            tmp = [1, rand(1,c-1), 0];
        end
        prototype = [tmp;1-tmp];
    end
    
    if Q>2
        tau(1:2,:) = prototype*(sum(prior(1:2)));
        tau(3:Q,:) = prior(3:Q)*ones(1,c+1);
    else
        tau = prototype;
    end
    % sanity check
    tau = theta_check(tau);
    
    %% Begin by E-step
    tau_mat_mod = tau(y+1,:);
    T_mat = P_S_P.*tau_mat_mod;
    T_mat = bsxfun(@times,T_mat,1./sum(T_mat,2));
    % normalize T_mat such that each line sums up to one
    % T_mat is an estimation of  P(s_i = s|p_i,y_i)
    
    k=0;
    DL=1;
    lik_old = likelihood_model(tau,y,p,P_S_P);
    
    %% Iterate
    while (k<=kmax)&&(DL>DL_eps)
        k = k+1;
        %[ktry, k]
        %% M-step Maximisation
        % Given T_mat, we get an estimation of the collusion model tau
        gamma = zeros(Q,c+1);
        for kq=1:Q
            ind = (y==kq-1);
            gamma(kq,:) = sum(T_mat(ind,:),1);
        end
        if ~symm
            tau = gamma;
            % Enforcing the basic rules
            tau(1,end) = 0;     %P(y=0|s=c) = 0
            tau(2,1) = 0;       %P(y=1|s=0) = 0
            if Q>2
                tau(3,1) = 0;    %P(y=2|s=0) = 0
                tau(3,end) = 0;  %P(y=2|s=c) = 0
            end
        else
            tau = zeros(Q,c+1);
            %1) P(y=0|s) = P(y=1|c-s)
            tmp = gamma(1,:)+fliplr(gamma(2,:));
            tau(1,:) = tmp;
            tau(2,:) = fliplr(tmp);
            tau(1,end) = 0;     %P(y=0|s=c) = 0
            tau(2,1) = 0;       %P(y=1|s=0) = 0
            
            %2) P(y=2|s) = P(y=2|c-s) for 0<s<c
            if Q>2
                tmp = gamma(3,:) + fliplr(gamma(3,:));
                tau(3,:) = tmp;
                tau(3,1) = 0;    %P(y=2|s=0) = 0
                tau(3,end) = 0;  %P(y=2|s=c) = 0
            end
            %3) P(y=3|s) = P(y=3|c-s) for 0<=s<=c
            if Q>3
                tmp = gamma(4,:) +fliplr(gamma(4,:));
                tau(4,:) = tmp;
            end
        end
        tau = bsxfun(@times,tau,1./sum(tau,1));
        
        %% E-Step Expectation
        % Given model tau, we get an estimation of P(s_i = s|p_i,y_i)
        % in a new T_mat
        tau_mat_mod = tau(y+1,:);
        T_mat = P_S_P.*tau_mat_mod;
        T_mat = bsxfun(@times,T_mat,1./sum(T_mat,2));
        % normalize T_mat such that each line sums up to one
        
        % Likelihood
        lik_new = likelihood_model(tau,y,p,P_S_P);
        DL = (lik_new-lik_old)/abs(lik_old);
        lik_old = lik_new;
        
    end
    %% Store if better
    if lik_new>likelihood
        model = tau;
        likelihood = lik_new;
        T_k = T_mat;
    end
end
