function d = accusation_score(x,opt_weight)
% function d = accusation_score(x,opt_weight)
%
% Computes the scores for the users whose codewords
% are stored in x, with weights pre-computed.
%
% Inputs:
% .x: user codewords (uint8)
% .opt_weight: structure containing arguments to compute the weights
% with fields
%   .W: For single decoder a [2 x m] matrix containing the weights
%       For aggregated decoder, this is a cell of K [2 x m] matrices
%       For packed codes, this is a [256 x m/8] matrix
%   .aggregation: A function handle which aggregates K scores into one.
%
% Outputs:
% .d: the score for the user codewords in x

if (DEBUG_FLAG>=3)
    disp('[accusation_score]: Computing the scores');
end
if ~isfield(opt_weight,'W') % if weights were not pre-computed
    error('Weights must be pre-computed first, Please use get_opt_single_decoder.m')
end

if ~iscell(opt_weight.W) % if single score
        d = disc_dis7(opt_weight.W,x,0);
else % multiple scores
    K = length(opt_weight.W);
    d_mat = zeros(size(x,2),K);
    for k=1:K
        d_mat(:,k) = disc_dis7(opt_weight.W{k},x,0);
    end
    d = opt_weight.aggregation(d_mat);
end
end





