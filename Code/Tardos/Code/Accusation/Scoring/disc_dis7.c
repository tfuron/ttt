
#include <stdio.h>
#include "mex.h"

#define uint8 unsigned char

void mexFunction (int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[])

{
  if (nrhs != 2 && nrhs != 3) 
    mexErrMsgTxt ("Invalid number of input arguments");
  
  if (nlhs > 1)
    mexErrMsgTxt ("This function output exactly 1 argument");

  int k = mxGetM (prhs[0]);
  int d = mxGetN (prhs[0]);
  int n = mxGetN (prhs[1]);

  if (mxGetM (prhs[1]) != d)
      mexErrMsgTxt("Dimension of tabulated distances are not consistent");

  if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS)
    mexErrMsgTxt ("first argument should double precision array"); 

  if (mxGetClassID(prhs[1]) != mxUINT8_CLASS)
    mexErrMsgTxt ("second argument should uint8 type"); 

  double * D = (double*) mxGetPr (prhs[0]);  /* tabulated distances */
  uint8 * x = (uint8*) mxGetPr (prhs[1]);    /* vectors */

  /* ouptut: distances */

  plhs[0] = mxCreateNumericMatrix (n, 1, mxDOUBLE_CLASS, mxREAL);
  double *dis = (double*) mxGetPr (plhs[0]);

  /* Defaut: manage matlab starting with 1: arithmetic pointer. 
     Otherwise use explicit offset parameters.              */
  int offset = 1; 
  if (nrhs == 3)
    offset = (int) mxGetScalar(prhs[2]);

  D = D - offset;
  
  int i, j, maxjk = d * k, jk;
  double distmp;
  


  for (i = 0 ; i < n ; i++) {
    distmp = 0;
    for (jk = 0 ; jk < maxjk ; jk += k)
      distmp += D[*(x++) + jk];
    dis[i] = distmp;
  }
}
