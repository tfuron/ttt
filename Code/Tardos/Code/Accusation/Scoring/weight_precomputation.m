function W = weight_precomputation(y,p,opt_weight)
% function W = weight_precomputation(y,p,opt_weight)
%
% Precomputes the weights for the accusation sum for a single decoder
%
% The weights are -log(P(Y|p)/P(Y|X,p)) (ML decoder)
% or Tardos/Skoric, Laarhoven, Oosterwijk, Desoubeaux, accusation functions
%
% Inputs
% .p: secret biases vector
% .y: pirated sequence
% .opt_weight: a structure containing the following fields
% Mandatory field
% .type: name of a single decoder
%       'desoubeaux','furon','ML','laarhoven','oosterwijk','tardos',
%
%
% Optional fields
% .theta: collusion model            [Q x c+1]
% .cmax: maximum number of colluders
% .CR: centered and reduced scores
% .coll: L known colluders sequences [m x L]

%% Define variables
m = length(p);
W = zeros(2,m);

%% Compute

switch lower(opt_weight.type)
    case 'tardos'
        %Tardos accusation functions symmetrized by Skoric
        Ind = (y <=1); % Focus on binary symbol {0,1} and ignore the rest
        dy = zeros(size(y));
        
        dy(Ind) = (1-2*double(y(Ind)));
        W(1,Ind) = (sqrt(p(Ind)./(1-p(Ind))).*dy(Ind))'; %x=0
        W(2,Ind) = (-sqrt((1-p(Ind))./p(Ind)).*dy(Ind))'; %x=1
    case 'ml'
        %Maximum Likelihood decoder
        c = size(opt_weight.theta,2)-1;
        Q = size(opt_weight.theta,1);
        Q_real = max(y)+1;
        
        if Q < Q_real % if there are symbols in y not foreseen in theta
            % Then this is a ugly tweak to cancel the weights
            % when this happens
            delta = Q_real-Q;
            theta = [opt_weight.theta;ones(delta,c+1)];
            %Ind = (y <= (size(theta,1)-1));
        else % everything is ok
            theta = opt_weight.theta;
        end
        
        if isfield(opt_weight,'coll')
            coll = opt_weight.coll;
            PS = ProbS_coll(p,c,coll)';
        else
            PS = ProbS_coll(p,c)';
            coll = [];
        end
        
        X1 = [coll,ones(m,1)];
        X0 = [coll,zeros(m,1)];
        
        % Compute the probability P(Y=y|p,theta)
        PY_mat = theta(y+1,:).*PS;
        PY_vec = sum(PY_mat,2);
        
        % Compute the probabilities P(Y=y|p,theta,X=0)
        PS_X0_mat = ProbS_coll(p,c,X0)';
        PY_X0_mat = theta(y+1,:).*PS_X0_mat;
        PY_X0_vec = sum(PY_X0_mat,2);
        
        % Compute the probabilities P(Y=y|p,theta,X=1)
        PS_X1_mat = ProbS_coll(p,c,X1)';
        PY_X1_mat = theta(y+1,:).*PS_X1_mat;
        PY_X1_vec = sum(PY_X1_mat,2);
        
        % Compute the weights
        W(1,:) = (log(PY_X0_vec) - log(PY_vec))';% x=0
        W(2,:) = (log(PY_X1_vec) - log(PY_vec))';% x=1
        if nargout>1
            PPS_X0 = PS_X0;
            PPS_X1 = PS_X1;
            PPS = PS;
        end
        
    case 'oosterwijk'
        % J.-J. Oosterwijk, B. Skoric, and J. Doumen
        % Optimal suspicion functions for Tardos traitor tracing schemes
        % 1st ACM Workshop on Information Hiding and Multimedia Forensics
        % IH&MMSec 2013
        
        W = -ones(2,m);
        W(1,y==0) = W(1,y==0) + 1./(1-p(y==0))'; %x=0
        W(2,y==1) = W(2,y==1) + 1./(p(y==1))'; %x=1
        
        
    case 'laarhoven'
        % T. Laarhoven
        % Capacities and Capacity-Achieving Decoders for Various Fingerprinting Games
        % http://arxiv.org/abs/1401.5688
        W = log(1-1/opt_weight.cmax)*ones(2,m);
        tmp = log(1+(p./(1-p))/opt_weight.cmax);
        W(1,y==0) = tmp(y==0)'; %x=0
        tmp = log(1+((1-p)./p)/opt_weight.cmax);
        W(2,y==1) = tmp(y==1)'; %x=1
        
        
    case 'desoubeaux'
        % M. Desoubeaux, C. Herzet, W. Puech, G. Le Guelvouit
        % Enhanced blind decoding of Tardos codes with new MAP-based
        % functions
        Ind = (y <=1); % Focus on binary symbol {0,1} and ignore the rest
        dy = zeros(size(y));
        dy(Ind) = (1-2*double(y(Ind)));
        c = opt_weight.c;
        
        W = zeros(2,m);
        W(1,Ind) = log((1+dy(Ind).*(1-p(Ind)).^(c-1))/2);  % x=0
        W(2,Ind) = log((1-dy(Ind).*p(Ind).^(c-1))/2); % x=1
        
        
    otherwise
        error('This type of decoder is unknown');
                
end


% Centering and scaling
% Applies only for linear scoring
if isfield(opt_weight,'CR')
    if opt_weight.CR
        % Centering so that E(Score_innocent) = 0
        mu = (W(1,:)*(1-p) + W(2,:)*p);
        W = W - (mu/m)*ones(size(W));
        % Reduction so that Var(Score_innocent) = m
        sig2 = (W(1,:).^2*(1-p) + W(2,:).^2*p);
        W = W*(sqrt(m)/sqrt(sig2));
    end
end
