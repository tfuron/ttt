function WW = weight_pack(W)
% function WW = weight_pack(W)
%
% Packing of the weight matrix to be used for packed codebook
% Packing by group of 8 bit symbols
%
% Input
% .W: weight matrix for unpacked code [2 x m]
%   W(1,j) = weight if x_j=0
%   W(2,j) = weight if x_j=1
%
% Output
% .WW: weight matrix for packed code [256 x m/8]

m = size(W,2);
if mod(m,8)~=0
    error('The code length must be a multiple of 8');
end
mp = m/8;
WW = zeros(2^8,mp);

symb_u = code_unpack(uint8(0:255));

for km=1:mp
    WW(:,km)= disc_dis7(W(:,(1:8)+(km-1)*8),symb_u,0)';
end
