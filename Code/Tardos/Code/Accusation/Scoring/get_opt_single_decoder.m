function new_opt = get_opt_single_decoder(y,p,opt)
% function new_opt = get_opt_single_decoder(y,p,opt)
%
% This gives back a structure containing all the options for the score
% computation of some popular single decoders
%
% Input
% .y: pirated sequence
% .p: secret sequence
% .opt: a structure containing options like
%   .type: method 'tardos', 'ML', 'oosterwijk', 'laarhoven', 'desoubeaux'
%               'ML_WCA', 'compound_WCA'
%   .cmax: the decoder bets on cmax colluders
%   .theta: a collusion model
%   .pack: if true, then the weights are packed
%
% Output
% .new_opt: a struture containing required arguments to compute score.
%   .type: method 'tardos', 'ML' ...
%   .cmax: the decoder bets on cmax colluders
%   .pack: if true, then the weights are packed
%   .W: weights in a [2 x m] matrix


if (DEBUG_FLAG>=3)
    disp('[get_opt_single_decoder]: Computing the weights');
end

new_opt = opt;
new_opt.p = p;
if ~isfield(opt,'pack')
    new_opt.pack = false;
end

switch lower(opt.type)
    case 'tardos'
        new_opt.W = weight_precomputation(y,p,new_opt);
        
    case 'ml'
        if ~isfield(opt,'cmax')
            if isfield(opt,'theta')
                new_opt.cmax = size(theta,2)-1;
            else
                new_opt.cmax = 10;
            end
        end
        if ~isfield(opt,'theta')
            new_opt.theta = collusion_model(new_opt.cmax,'uniform');
        end
        new_opt.W = weight_precomputation(y,p,new_opt);
        
    case 'oosterwijk'
        new_opt.W = weight_precomputation(y,p,new_opt);
    case 'laarhoven'
        if ~isfield(opt,'cmax')
            new_opt.cmax = 10;
        end
        new_opt.W = weight_precomputation(y,p,new_opt);
        
    case 'desoubeaux'
        if ~isfield(opt,'cmax')
            new_opt.cmax = 10;
        end
        if ~isfield(opt,'n')
            new_opt.n = 10^6;
        end
        new_opt.W = cell(new_opt.cmax-1,1);
        %dy = (1-2*double(y));
        %K = zeros(1,new_opt.cmax-1);
        opt_weight = opt;
        for kc=2:new_opt.cmax
            opt_weight.type = 'desoubeaux';
            opt_weight.c = kc;
            opt_weight.n = new_opt.n;
            new_opt.W{kc-1} = weight_precomputation(y,p,opt_weight);
            new_opt.W{kc-1}(:,1) = new_opt.W{kc-1}(:,1)+log(kc/opt_weight.n);
            %K(kc-1) = log((new_opt.n-kc)/new_opt.n) + sum(log((1+dy.*((1-p).^kc-p.^kc))/2));
        end
        %KK = max_log(K);
        new_opt.aggregation = @(x) max_log(x);%-KK;
        
    case 'ml_wca'
        if ~isfield(opt,'cmax')
            new_opt.cmax = 10;
        end
        opt_weight.verbose = false;
        opt_weight.theta = collusion_model_I(new_opt.cmax,'wca_simple',opt_weight);
        opt_weight.type = 'ml';
        
        new_opt.W = weight_precomputation(y,p,opt_weight);
        
        
    case 'compound_wca'
        if ~isfield(opt,'cmax')
            new_opt.cmax = 10;
        end
        new_opt.W = cell(new_opt.cmax-1,1);
        %opt_weight = opt;
        opt_weight.type = 'ml';
        opt_weight.verbose = false;
        for kc=2:new_opt.cmax
            %opt_weight.c = kc;
            opt_weight.theta = collusion_model_I(kc,'wca_simple',opt_weight);
            new_opt.W{kc-1} = weight_precomputation(y,p,opt_weight);    
        end

        new_opt.aggregation = @(x) max(x,[],2);
        
        
    otherwise
        error('This type of decoder is unknown');
        
        
        %
        %     case 'compound'
        %         if ~isfield(opt,'cmax')
        %             new_opt.cmax = 10;
        %         end
        %         new_opt.W = cell(new_opt.cmax-1,1);
        %         for kc=2:new_opt.cmax
        %             opt_weight = opt;
        %             opt_weight.type = 'ML';
        %             opt.verbose = false;
        %             opt_weight.theta = collusion_model(kc,'wca_simple',opt);
        %             new_opt.W{kc-1} = weight_precomputation(opt_weight);
        %         end
        %         new_opt.aggregation = @(x) max(x,[],2);
        
end

if new_opt.pack==true
    new_opt.W = weight_pack(new_opt.W);
end

end

function Y = max_log(X)
Xmax = max(X,[],2);
Y = Xmax + log(sum(exp(X-repmat(Xmax,1,size(X,2))),2));
end

