function [p,w] = f_saddlepoint_joint(c)
%function [p,w] = f_saddlepoint_joint(c)
%
% Optimal discrete distribution against c colluders
% This is the distribution giving a saddle point of the rate of
% a joint decoder
%
% Data from "High-rate fingerprinting codes and the fingerprinting
% capacity", Ehsan Amiri & Gabor Tardos, SODA'09
%
% Input
% .c: number of colluders
%
% Output
% .p: value of the secret bias
% .w: probability mass function

switch c
    case 1
        p = 1/2;
        w = 1;
    case 2
        p = 1/2;
        w = 1;
    case 3
        p = [0.26 ; 0.74];
        w = [1/2 ; 1/2];
    case 4
        p = [0.227 ; 0.773];
        w = [1/2 ; 1/2];
    case 5
        p = [0.18 ; 0.82];
        w = [1/2 ; 1/2];
    case 6
        p = [0.15 ; 0.85];
        w = [1/2 ; 1/2];
        %case 7
        %p = [0.13 ; 0.5; 0.87];
    otherwise
        error('Not implemented')
end
