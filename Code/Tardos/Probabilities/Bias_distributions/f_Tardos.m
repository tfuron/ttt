function y = f_Tardos(x)
% function y = f_Tardos(x)
%
% Tardos arcsine pdf
% without cut-off parameter

y = 1./sqrt(x.*(1-x))/pi;

