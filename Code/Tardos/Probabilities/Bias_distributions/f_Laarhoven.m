function [p,w] = f_Laarhoven(c)
%function [p,w] = f_Laarhoven(c)

% A kind of discrete Arcsine distribution as an approximation
% of Nuida's distribution (based on Gauss-Legendre quadrature)
%
% Input
% .c: number of colluders
%
% Output
% .p: values of the secret biases
% .w: probability mass function
%
% Reference
% Discrete Distribution in the Tardos Scheme, Revisited
% T. Laarhoven and B. de Weger, IHMMSEC 2013

p = (sin((4*(1:c)-1)*pi/(8*c+4))).^2;
w = ones(size(p))/c;

end

