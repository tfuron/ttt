function [p,w] = f_saddlepoint_single(c)
%function [p,w] = f_saddlepoint_single(c)
%
% Optimal discrete distribution against c colluders
% This is the distribution giving a saddle point of the rate of
% a single decoder
%
% Data from "CAPACITY-ACHIEVING FINGERPRINT DECODING", Yen-Wei Huang, Pierre Moulin
% IEEE WIFS 2009, London
%
% Input
% .c: number of colluders
%
% Output
% .p: value of the secret bias
% .w: probability mass function

switch c
    case 1
        p = 1/2;
        w = 1;
    case 2
        p = 1/2;
        w = 1;
    case 3
        p = [0.1753 ; 0.8247];
        w = [1/2 ; 1/2];
    case 5
        p = [0.0856 ; 0.5 ; 0.9144];
        w = [0.3515 ; 0.2970 ; 0.3515];
    case 12
        p = [0.02 ; 0.15 ; 0.37 ; 0.63 ; 0.85 ; 0.98];
        w = [0.20 ; 0.15 ; 0.15 ; 0.15 ; 0.15 ; 0.20];
    otherwise
        error('Not implemented')
end