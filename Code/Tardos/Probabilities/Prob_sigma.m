function P = Prob_sigma(s,c,a,b)
%function P = Prob_sigma(s,c,a,b)
%
% Probability that sigma=s over c symbols
% are equal to one.
% when the prior is the Beta distrbution B(p,a,b)

P = nchoosek(c,s)*beta(a+s,b+c-s)/beta(a,b);
