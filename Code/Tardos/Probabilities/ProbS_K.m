function P = ProbS_K(p,c,K,L,psi)
% function P = ProbS_K(p,c,K,L,psi)
%
% Probability of oberserving s '1' over c symbols
% knowing L<c colluders have K '1'
%
% 1st method: binomial distribution
% Inputs:
% p: secret biases vector       [1 x m]
% c: number of colluders        [1 x 1]
% K: already have K '1' over L  [1 x 1]
% L: L known colluders          [1 x 1]
%
% 2nd method: Noncentral Hypergeometric distribution
% Taking into account the code that has been generated
% p: number of 0/1 per index    [2 x m]
% p(:,1): number of 0
% p(:,2): number of 1
% psi:  a priori ratio of being colluder when having a '0'/'1'
% psi = P0(1-P1)/P1/(1-P0)
%
% Output:
% P: Proba in matrix            [(c+1) x m]

if nargin<2
    error('Give me at least two inputs');
end
if nargin==2 % Probability of s '1' out of c
    L=0;
    K=0;
end
if nargin==3 % Probability of s '1' out of c knowing x
    L = 1;
end
if nargin>3 % Probability Sigma=s knowing p and X=k
    if L>c
        error('L must be lower or equal c');
    end
    if K>L
        error('K must be lower or equal than L')
    end
end


[method, m] = size(p);
s = 0:c;
P = zeros(c+1,m);
switch method
    case 1 % Binomial distribution
        for jnd=1:c+1
            sk = s(jnd)-K;
            if (sk<0)||(sk>c-L)
                P(jnd,:)=0;
            else
                P(jnd,:) = nchoosek(c-L,sk)*p.^(sk).*(1-p).^(c-L-sk);
            end
        end
    case 2 % Noncentral HG distribution
        % Translate our setup in HG notations
        n1 = p(2,:); % number of '1'
        n2 = p(1,:); % number of '0'
        m1 = c;
        if nargin < 5 % equal probability of being colluder given the symbol
            psi = ones(size(n1)); 
        end
        
        P(K+1+(0:(c-L)),:) = Prob_HG(n1-K,n2-(L-K),psi,c-L);
    otherwise
        error('Unknown format of p value');
end
P = P./repmat(sum(P,1),c+1,1);











