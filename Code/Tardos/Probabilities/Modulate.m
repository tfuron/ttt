function Pout = Modulate(Pin,y,deriv)
%function Pout = Modulate(Pin,y,deriv)
% Modulate function
% Pin are probabilities for y=1
% Pout are probabilities for y
% If deriv = 0 -> Pout = Pin (y=1) OR 1-Pin (y=0)
% If deriv = 1 -> Pout = Pin (y=1) OR -Pin (y=0)

Pout = zeros(size(Pin));
[lin col] = size(Pin);
if col==length(y)
    Ymat = repmat(double(y),lin,1);
elseif lin==length(y)
    Ymat = repmat(double(y),1,col);
else
    error('Dimension mismatch')
end
if nargin<3
    deriv = false;
end
if deriv
    Pout  = (2*Ymat-1).*Pin;            %Pout = Pin (y=1) OR -Pin (y=0)
else
    Pout  = (2*Pin-1).*Ymat + 1 - Pin;  %Pout = Pin (y=1) OR 1-Pin (y=0)
end
