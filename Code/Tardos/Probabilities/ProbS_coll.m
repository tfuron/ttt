function P=ProbS_coll(p,c,coll,psi)
% function P=ProbS_coll(p,c,coll,psi)
%
% Probability of oberserving s '1' over c symbols
% knowing
% - bias p
% - some colluders sequences coll are known
%
% Inputs:
% .p: secret biases vector [1 x m]
% .c: number of colluders
% .coll: sequences of known colluders [L x m]
% .psi: if hypergeometric model is selected
%
% Outputs
% P: Proba in matrix [m x (c+1)]


% Check the input
m = length(p);

if nargin<2
    error('Give me at least two inputs');
end
if nargin==2 % Probability y='1' knowing p and theta
    L=0;
    sum_coll=zeros(1,m);
end
if nargin==3 % Probability Sigma=s knowing p, theta and X=k
    [mc, L] = size(coll);
    if (mc>0)&&(m~=mc)
        error('lengths of vectors do not match')
    end
    if L==0
        sum_coll = zeros(1,m);
    else
        sum_coll = sum(coll,2)';
    end
end
if L>c
    error('The number of known colluders must be strictly lower than c');
end

P = zeros(c+1,m);
if exist('psi','var')
    for j = 1:m
        P(:,j) = ProbS_K(p(j),c,sum_coll(j),L,psi);
    end
else
    for j = 1:m
        P(:,j) = ProbS_K(p(j),c,sum_coll(j),L);
    end
end

