function P=ProbS_P(p,s,c)
%%%%%%%%%%%%
% Deprecated!!!!
% Please, use ProbS_K.m 
%%%%%%%%%%%%%% 

% function P=ProbS_P(p,s,c)
%
% Probability of oberserving s '1'
% over c symbols
% knowing bias p

if c<15
P = nchoosek(c,s)*p.^s.*(1-p).^(c-s);
else
   % From "Fast and Stable algorithms for computing and sampling from the
   % noncentral hypergeometric distribution"
   % Liao and Rosen
   % The american statistician, Vol. 55, No. 4 (Nov., 2001)
   
   P = zeros(size(p))
   mode = floor((c+1)*p);
   
   
end