function P = ProbY_K(p,theta,K,L)
%function P = ProbY_K(p,theta,K,L)
%
% Probability that Y = y symbol 
%
% Inputs:
% .p: bias probability in (0,1) [1 x m]
% .theta: collusion model [Q x c+1]
% .K: K users have already a '1' ...
% .L: ... over L known colluders
%
% Outputs:
% .P: Probability that Y = y symbol [Q x m]

% Check the inputs
narginchk(2, 4);
c = size(theta,2)-1;
Q = size(theta,1);
m = length(p);
if nargin==2 % Probability Y = y knowing p and theta
    L=0;
    K=0;
end
if nargin==3 % Probability Y = y knowing p, theta and X=K over L=1
    L=1;
end
if K>L
    error('K must be lower or equal than L');
end
if L>c
    error('L must be lower or equal than c');
end

% for loop
P = zeros(Q,m);
for sk = 0:c-L
   % the remaining c-K colluders have sk '1'  
   P = P + theta(:,sk+K+1)*(nchoosek(c-L,sk)*p.^(sk).*(1-p).^(c-L-sk));
   %  = [Q x 1].[1 x m]
end
