function y = Prob_check(P)
% Clip P so that it lies in [0,1]

y = P;
y(y>1)=1;
y(y<0)=0;
