function M = MI_binary(px,py_x0,py_x1)
%function M = MI_binary(px,py_x0,py_x1)
%
% Mutual Information in nats between two binary random variables x and y.
%
% Inputs
% .px: X~Bernoulli(px), ie. px = Prob that X=1 
% .py_x0: Prob that Y=1 knowing X=0
% .py_x1: Prob that Y=1 knowing X=1
%
% px, py_x0, py_x1 have the same size
%
% Outputs
% .M: mutual information in nats [same size as px]

if norm(size(px)-size(py_x0))~=0||norm(size(px)-size(py_x1))~=0
    error('Inputs must have the same size');
end


M = zeros(size(px));
M = H_binary((1-px).*py_x0 + px.*py_x1) - ((1-px).*H_binary(py_x0)+px.*H_binary(py_x1));
