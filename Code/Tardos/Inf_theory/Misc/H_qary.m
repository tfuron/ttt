function H = H_qary(px)
% function H = H_qary(px)
%
% Entropy in nats of a discrete random variable taking Q values
%
% Inputs:
% .px: probability mass function [Q x L]
%
% Outputs:
% .H: Entropy in nats [1 x L]

if sum(abs(sum(px,1)-1))>10^-3
    error('px does not sum up to 1');
end

h = -px.*log(px+realmin);
H = sum(h,1);
