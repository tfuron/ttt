function H = H_binary(px)
%function H = H_binary(px)
%
% Entropy in nats of a binary random variable.
%
% Input
% .px: X~Bernoulli(px), ie. px = Prob that x =1
%
% Ouput
% .H: Entropy in nats [same size as px]

H = zeros(size(px));
H = real(-px.*log(px)-(1-px).*log(1-px));
H(px==0)=0;
H(px==1)=0;


