function D=DKL_discrete(px,py)
% function DKL_discrete(px,py)
%
% Kullback Leibler distance in nats
% between two random variables
%
% Inputs:
% .px: Prob that X = (a_1,a_2,...,a_Q)
% .py: Prob that Y = (a_1,a_2,...,a_Q)
%
% Comments:
% .If px is [Q x m] (and so is py), then m DKL values are computed
% .If px is [1 x m] (and so is py), then X is a binary r.v.
%
% Output
% .D: distance in nats [1 x m]


if norm(size(px)-size(py))~=0
    error('px and py should have the same size');
end
if nnz((px<0)|(px>1))>0
    error('px is not a probability')
end
if nnz((py<0)|(py>1))>0
    error('py is not a probability')
end

[n, m]=size(px);
D = zeros(1,m);

if n==1
    if nnz((px<0)|(px>1))>0
        error('px is not a probability')
    end
    if nnz((py<0)|(py>1))>0
        error('py is not a probability')
    end
    D = px.*log((px+realmin)./py)+(1-px).*log((1-px+realmin)./(1-py));
else
    if nnz(abs(sum(px)-1)>10^-6)>0
        error('px is not a probability distribution')
    end
    D_tmp = px.*log((px+realmin)./py);
    D = sum(D_tmp);
end




