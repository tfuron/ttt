function D=DKL_binary(px,py)
% function D = DKL_binary(px,py)
%
% Kullback Leibler distance
% between two random binary variable
% 
% Inputs
% .px: X~Bernoulli(px), ie. px = Prob that X=1
% .py: Y~Bernoulli(py), ie. py = Prob that Y=1
%
% These can be m different distributions and the output is then a vector
%
% Output
% .D: Distance in nats

if norm(size(px)-size(py))~=0
    error('px and py should have the same size');
end

D = zeros(size(px));

D = px.*log(px./py)+(1-px).*log((1-px)./(1-py));

Ix = find(px==0);
D(Ix) = -log((1-py(Ix)));
Ix = find(px==1);
D(Ix) = -log(py(Ix));

