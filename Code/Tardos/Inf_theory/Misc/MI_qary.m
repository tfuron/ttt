function M = MI_qary(px,py_x0,py_x1)
%function M = MI_qary(px,py_x0,py_x1)
%
% Mutual Information in nats between two binary random variables x and y,
% x being binary and y being q-ary
%
% Inputs:
% .px: X~Bernoulli(px), ie px = Prob that X=1 [1 x L]
% .py_x0: Prob that Y=y knowing X=0 [Q x L]
% .py_x1: Prob that Y=y knowing X=1 [Q x L]
%
% Comments:
% .If Q=1, then Y is binary (see MI_binary.m)

if nnz(size(py_x0)-size(py_x1))>0
    error('Inputs py_x0 and py_x1 must have the same size');
end

%tmpx = px(:);
L = length(px);
if (L~=size(py_x0,2))||(L~=size(py_x1,2))
    error('Inputs py_x0 and py_x1 must have the same length as px');
end

Q = size(py_x0,1);
if Q==1
    M = MI_binary(px,py_x0,py_x1);
else
    ppx = repmat(px,Q,1);
    M = H_qary((1-ppx).*py_x0 + ppx.*py_x1)...
        -((1-px).*H_qary(py_x0)+px.*H_qary(py_x1));
end