function R=Rate_simple_disc(p,w,theta,k)
%function R=Rate_simple_disc(p,w,theta,k)
%
% Rate in nats of a simple decoder
% for a given discrete pdf f of time-sharing variable p
% f = sum w_i * delta(p_i) and collusion model theta
%
% Inputs
% .p: bias values for p  [m x 1]
% .w: probabilities Prob(P=p) [m x 1]
% .theta: collusion model [Q x (c+1)]
% .k (optional): k colluders have been identified (conditioned decoder)
%
% Output
% .R: Rate in nats

% Check inputs
narginchk(3, 4);
if length(p)~=length(w)
    error('First two arguments p and w must have the same length');
end
if abs(1-sum(w))>10^-6
    error('Weight should sum up to one');
end
if (max(p)>1)||(min(p)<0)
    error('Variable p must be in [0,1]')
end
if min(w)<0
    error('weight must be positive')
end

% Sum up the rate
if nargin==3
    R = sum(w.*MI_qary(p',ProbY_K(p',theta,0),ProbY_K(p',theta,1))');
else
    if k>=(size(theta,2)-1)
       error('k known colluders must be lower than c'); 
    end
    R = 0;
    for j=0:k
        R = R + sum(w.*MI_qary(p',ProbY_K(p',theta,j,k+1),ProbY_K(p',theta,j+1,k+1))');
    end
end