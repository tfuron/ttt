function R=Rate_joint_cont(f,theta,K)
%function R=Rate_joint_cont(f,theta,K)
%
% Rate in nats of a joint decoder
% for a given continuous pdf f of time-sharing variable p
% and collusion model theta
%
% Inputs
% .f: pdf of p function handle
% .theta: collusion model [Q x (c+1)]
% .K (optional): K-uple decoder (K<c)
%     default is K=c
%
% Output
% .R: Rate in nats
%
% Example
% R = Rate_joint_cont(@f_Tardos,collusion_model_I(5,'uniform'))


narginchk(2, 3);

if ~isa(f,'function_handle')
    error('First argument should be a function handle')
end

if abs(quadgk(f,0,1)-1)>10^-6
    error('First argument should sum up to one')
end

c = size(theta,2)-1;
if nargin==2 % rate of joint decoder
    R = integral(@(p)f(p).*H_qary(ProbY_K(p,theta)),0,1);%H(Y)
    for kj = 0:c % H(Y|K)
        R=R-H_qary(theta(:,kj+1))*nchoosek(c,kj)*integral(@(p)f(p).*p.^(kj).*(1-p).^(c-kj),0,1);
    end
    R = R/c;
else % rate of a K-joint decoder
    if K>c
        error('K must be <= c');
    end
    R = integral(@(p)f(p).*H_qary(ProbY_K(p,theta)),0,1);
    for kj = 0:K % H(Y|K)
        R = R - nchoosek(K,kj)*integral(@(p)f(p).*p.^(kj).*(1-p).^(K-kj)...
            .*hq(kj,K,theta,p),0,1);
    end
    R = R/K;
end
end

%% subfunction
function y = hq(kj,K,theta,p)
c = size(theta,2)-1;
pi_kc = ProbS_K(p,c,kj,K); % Prob of s-kj '1' over (c-K) remaining colluders
y = H_qary(theta*pi_kc); % [Qx(c+1)]*[(c+1)xm] -> [1xm]
end
