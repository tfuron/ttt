function R=Rate_joint_disc(p,w,theta,K)
%function R=Rate_joint_disc(p,w,theta,K)
%
% Rate in nats of a joint decoder
% for a given discrete pdf f of time-sharing variable p
% f = sum w_i * delta(p_i)
% and collusion model theta for c colluders
%
% Inputs
% .p: bias values for p  [m x 1]
% .w: probabilities Prob(P=p) [m x 1]
% .theta: collusion model [Q x (c+1)]
% .K (optional): K-uple decoder (K<c)
%     default is K=c
%
% Output
% .R: Rate in nats

narginchk(3, 4);

if norm(size(p)-size(w))~=0
    error('first two argument must have the same size');
end

if abs(1-sum(w))>10^-6
    error('Weight should sum up to one');
end

if (max(p)>1)||(min(p)<0)
    error('Variable p must be in [0,1]')
end

if min(w)<0
    error('weight must be positive')
end

c = size(theta,2)-1;
if nargin==3 % rate of joint decoder K=c
    R = sum(w'.*H_qary(ProbY_K(p',theta))); %H(Y)
    for kj = 0:c % H(Y|K)
        R=R-H_qary(theta(:,kj+1))*nchoosek(c,kj)*sum(w.*p.^(kj).*(1-p).^(c-kj));
    end
    R = R/c;
else % rate of a K-joint decoder
    if K>c
        error('K must be <= c');
    end
    R = sum(w.*H_qary(ProbY_K(p',theta))');
    for kj = 0:K % H(Y|K)
        R = R - nchoosek(K,kj)*sum(w.*p.^(kj).*(1-p).^(K-kj).*hq(kj,K,theta,p')');
    end
    R = R/K;
end

end

%% subfunction
function y = hq(kj,K,theta,p)
c = size(theta,2)-1;
pi_kc = ProbS_K(p,c,kj,K); % Prob of s-kj '1' over (c-K) remaining colluders
y = H_qary(theta*pi_kc); % [Qx(c+1)]*[(c+1)xm] -> [1xm]
end




