function R=Rate_simple_cont(f,theta,k)
%function R=Rate_simple_cont(f,theta,k)
%
% Rate in nats of a simple decoder
% for a given continuous pdf f of time-sharing variable p
% and collusion model theta
%
% Inputs
% .f: pdf of p function handle
% .theta: collusion model [Q x (c+1)]
% .k (optional): k colluders have been identified (conditioned decoder)
%
% Output
% .R: Rate in nats
%
% Example
% R = Rate_simple_cont(@f_Tardos,collusion_model_I(5,'uniform'))

narginchk(2, 3);

if ~isa(f,'function_handle')
    error('First argument should be a function handle')
end

if abs(quadgk(f,0,1)-1)>10^-6
    error('First argument should sum up to one')
end

if nargin==2 % rate of simple decoder
    R = integral(@(p)f(p).*MI_qary(p,ProbY_K(p,theta,0),...
        ProbY_K(p,theta,1)),0,1);
else % rate of a conditioned simple decoder
    if k>=(size(theta,2)-1)
       error('k known colluders must be lower than c'); 
    end
    R = 0;
    for j=0:k
        R = R + integral(@(p)f(p).*MI_qary(p,ProbY_K(p,theta,j,k+1),...
            ProbY_K(p,theta,j+1,k+1)),0,1);
    end
end

