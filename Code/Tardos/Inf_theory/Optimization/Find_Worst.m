function [theta,Rmin] = Find_Worst(c,varargin)
% function theta = Find_Worst(c,options)
%
% Find the worst collusion attack
% ie. the one minimizing the rate
%
% Inputs:
% .c: number of colluders
% .varargin: optional parameters
%(stored in a structure with following fields)
%
% Options:
% .f: continuous pdf f of time-sharing variable p           [@f_Tardos]
% .simple: true simple decoder / false joint decoder        [true]
% .symmetric: true symmetric collusion model                [true]
% .precomputed: true takes precomputed models if existing   [true]
% .continuous: true / false discrete                        [true]
%   if false
%       .p: values of p_i                                   [1/2]
%       .w: weights (sum up to one)                         [1]
%
% .conditioned: true colluders already disclosed            [false]
%   if true
%       .K: number of disclosed colluders                   [1]
% .verbose: displays messages

%%%%%%%%%%%%%%%%%
% Parsing the arguments
%%%%%%%%%%%%%%%%%%
nargoutchk(0,2);
Ntry = 10;

% Create an instance of the inputParser class.
p = inputParser;
% Define inputs that one must pass on every call:
p.addRequired('c', @(x)validateattributes(x,{'numeric'},{'scalar','integer', 'positive'}));

p.addParamValue('f',@f_Tardos,@(x)isa(x,'function_handle'));
p.addParamValue('simple',true,@(x)islogical(x));
p.addParamValue('continuous',true,@(x)islogical(x));
p.addParamValue('conditioned',false,@(x)islogical(x));
p.addParamValue('symmetric',true,@(x)islogical(x));
p.addParamValue('verbose',true,@(x)islogical(x));
p.addParamValue('precomputed',true,@(x)islogical(x));

% if discrete, these are the values and their weights
p.addParamValue('p',1/2,@(x)validateattributes(x,{'numeric'},{'positive','vector','>=', 0, '<=', 1}));
p.addParamValue('w',1.0,@(x)validateattributes(x,{'numeric'},{'positive','vector'}));%&&(abs(sum(x)-1)<10^-6));

% if conditioned, this is the number of identified colluders
p.addParamValue('K',1,@(x)validateattributes(x,{'numeric'},{'scalar,''integer', 'positive'}));

% Parse and validate all input arguments.
p.StructExpand = true;
p.KeepUnmatched = true;
p.parse(c,varargin{:});

h = p.Results;
if h.conditioned==false
    h.K=0;
end
% Display the names of all arguments.
if h.verbose
    fprintf('\n')
    disp '[Find_Worst]: List of all arguments'
    disp(p.Results)
end


%%%%%%%%%%%%%%%%%%
% Pre-computed Worst Case Attack
% for binary collusion model [2 x (c+1)]
% for Tardos arcsine distribution
%%%%%%%%%%%%%%%%%%
if (strcmpi(func2str(h.f),'F_Tardos')&&(h.simple)&&(h.conditioned==false)...
        &&(c<13)&&(h.precomputed))
    C{1} = 1;
    C{2} = [0, 0.5, 1];
    C{3} = [0, 0.6513, 0.3487, 1];
    C{4} = [0, 0.4875, 0.5, 0.5125, 1];
    C{5} = [0, 0.5935, 0, 1, 0.4065, 1];
    C{6} = [0, 0.5021, 0.1752, 0.5, 0.8248, 0.4979, 1];
    % From now on, the results are approximative
    C{7} = [0, 0.5009, 0.0015, 0.8142, 0.1858, 0.9985, 0.4991, 1];
    C{8} = [0, 0.4694, 0, 0.6888, 0.5, 0.3112, 1, 0.5306, 1];
    C{9} = [0, 0.4360, 0, 0.6966, 0.2273, 0.7727, 0.3034, 1, 0.5640, 1];
    C{10} = [0, 0.4076, 0, 0.7315, 0.1538, 0.5, 0.8462, 0.2685, 1, 0.5924, 1];
    C{11} = [0, 0.4003, 0, 0.5115, 0.4506, 0.3001, 0.6999, 0.5494, 0.4885, 1, 0.5997, 1];
    C{12} = [0, 0.3987, 0, 0.5044, 0.5355, 0.2145, 0.5, 0.7855, 0.4645, 0.4956, 1, 0.6013, 1];
    if h.verbose
        disp('[Find_Worst]: Pre-computed');
    end
    theta = [1-C{c},C{c}];
    Rmin = Rate_simple_cont(h.f,theta,0);
else
    %%%%%%%%%%%%%%%%%%%%
    % Defining the optimization problem
    %%%%%%%%%%%%%%%%%%%%
    optprob.Aineq = [];
    optprob.bineq = [];
    optprob.Aeq = [];
    optprob.beq = [];
    optprob.nonlcon = [];
    optprob.solver = 'fmincon';
    optprob.options = optimset;
    optprob.options.Display = 'off';
    optprob.options.Algorithm = 'active-set';
    %optprob.options = optimset('GradObj','on','Hessian','user-supplied');
    
    
    if h.symmetric==true
        if c==2
            theta = [0 1/2 1. 1 1/2 0];
            if h.continuous==true
                Rmin = Rate_simple_cont(h.f,theta,h.K);
            else
                Rmin = Rate_simple_disc(h.p,h.w,theta,h.K);
            end
            return
        end
        l_reduced = ceil((h.c-2)/2);
        L = l_reduced;  % dimension of the problem
        unpack =@(x) theta_unfold(c,x);
    else
        L = h.c-1;    % dimension of the problem
        unpack =@(x) [0, x, 1];
    end
    optprob.lb = zeros(1,L);
    optprob.ub = ones(1,L);
    
    if h.simple==true
        if h.continuous==true
            optprob.objective = @(x)Rate_simple_cont(h.f,unpack(x),h.K);
        else
            optprob.objective = @(x)Rate_simple_disc(h.p,h.w,unpack(x),h.K);
        end
    else
        if h.continuous==true
            optprob.objective = @(x)Rate_joint_cont(h.f,unpack(x));
            % Replace with Blahut-Arimoto
            % TBD: with side-info h.K
        else
            error('Find_Worst: Not yet implemented')
        end
    end
    
    %%%%%%%%%%%%%%
    % Several rounds to avoid local minimum
    %%%%%%%%%%%%%%
    Rmin = +Inf;
    
    for k=1:Ntry
        optprob.x0 = rand(1,L);
        %keyboard
        [th,fmin]=fmincon(optprob);
        if fmin<Rmin
            Rmin = fmin;
            model_tmp = th;
        end
    end
    theta = unpack(model_tmp);
end
return


