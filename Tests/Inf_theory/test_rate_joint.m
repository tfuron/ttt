%% Test for the rate of decoders
%% with discrete bias distribution
% comparison between:
%  .Iterative joint decoder (from single, to pairs, triplets...)
%  .Iterative single decoder (identifying a 1st colluder, 2nd, 3rd ...)


c = 12;

%% Discrete bias distribution
[p, w] = f_saddlepoint_single(c);

% Uniform attack
theta = collusion_model_I(c,'Uniform');
opt_collusion.erasure_rate = 0.2;
theta_e = collusion_model_I(c,'Uniform',opt_collusion);

Rate_j = zeros(c,1);
Rate_s = zeros(c,1);
Rate_je = zeros(c,1);
Rate_se = zeros(c,1);
for k=1:c
    Rate_j(k) = Rate_joint_disc(p,w,theta,k);
    Rate_je(k) = Rate_joint_disc(p,w,theta_e,k);
    if k==1
        Rate_s(k) = Rate_simple_disc(p,w,theta);
        Rate_se(k) = Rate_simple_disc(p,w,theta_e);
    else
        Rate_s(k) = Rate_simple_disc(p,w,theta,k-1);
        Rate_se(k) = Rate_simple_disc(p,w,theta_e,k-1);
    end
end

figure(1)
plot(1:c,Rate_j,'b',1:c,Rate_s,'r',...
    1:c,Rate_je,':b',1:c,Rate_se,':r')
ylabel('rate in nats');
xlabel('k-user joint decoder');
title(['Discrete bias distribution / Uniform attack / c =',num2str(c)]);
legend('Iterative joint','Iterative single',...
    'Iter. joint erasure','Iter. single erasure',...
    'Location','Best')


% Majority attack
theta = collusion_model_I(c,'Majority');
theta_e = collusion_model_I(c,'Majority',opt_collusion);

Rate_j = zeros(c,1);
Rate_s = zeros(c,1);
Rate_je = zeros(c,1);
Rate_se = zeros(c,1);
for k=1:c
    Rate_j(k) = Rate_joint_disc(p,w,theta,k);
    Rate_je(k) = Rate_joint_disc(p,w,theta_e,k);
    if k==1
        Rate_s(k) = Rate_simple_disc(p,w,theta);
        Rate_se(k) = Rate_simple_disc(p,w,theta_e);
    else
        Rate_s(k) = Rate_simple_disc(p,w,theta,k-1);
        Rate_se(k) = Rate_simple_disc(p,w,theta_e,k-1);
    end
end

figure(2)
plot(1:c,Rate_j,'b',1:c,Rate_s,'r',...
    1:c,Rate_je,':b',1:c,Rate_se,':r')
ylabel('rate in nats');
xlabel('k-user joint decoder');
title(['Discrete bias distribution / Majority attack / c =',num2str(c)]);
legend('Iterative joint','Iterative single',...
    'Iter. joint erasure','Iter. single erasure',...
    'Location','Best')


%% Continuous bias distribution
% Uniform attack
theta = collusion_model_I(c,'Uniform');
opt_collusion.erasure_rate = 0.2;
theta_e = collusion_model_I(c,'Uniform',opt_collusion);

Rate_j = zeros(c,1);
Rate_s = zeros(c,1);
Rate_je = zeros(c,1);
Rate_se = zeros(c,1);
for k=1:c
    Rate_j(k) = Rate_joint_cont(@f_Tardos,theta,k);
    Rate_je(k) = Rate_joint_cont(@f_Tardos,theta_e,k);
    if k==1
        Rate_s(k) = Rate_simple_cont(@f_Tardos,theta);
        Rate_se(k) = Rate_simple_cont(@f_Tardos,theta_e);
    else
        Rate_s(k) = Rate_simple_cont(@f_Tardos,theta,k-1);
        Rate_se(k) = Rate_simple_cont(@f_Tardos,theta_e,k-1);
    end
end

figure(3)
plot(1:c,Rate_j,'b',1:c,Rate_s,'r',...
    1:c,Rate_je,':b',1:c,Rate_se,':r')
ylabel('rate in nats');
xlabel('k-user joint decoder');
title(['Continuous bias distribution / Uniform attack / c =',num2str(c)]);
legend('Iterative joint','Iterative single',...
    'Iter. joint erasure','Iter. single erasure',...
    'Location','Best')


% Majority attack
theta = collusion_model_I(c,'Majority');
theta_e = collusion_model_I(c,'Majority',opt_collusion);

Rate_j = zeros(c,1);
Rate_s = zeros(c,1);
Rate_je = zeros(c,1);
Rate_se = zeros(c,1);
for k=1:c
    Rate_j(k) = Rate_joint_cont(@f_Tardos,theta,k);
    Rate_je(k) = Rate_joint_cont(@f_Tardos,theta_e,k);
    if k==1
        Rate_s(k) = Rate_simple_cont(@f_Tardos,theta);
        Rate_se(k) = Rate_simple_cont(@f_Tardos,theta_e);
    else
        Rate_s(k) = Rate_simple_cont(@f_Tardos,theta,k-1);
        Rate_se(k) = Rate_simple_cont(@f_Tardos,theta_e,k-1);
    end
end

figure(4)
plot(1:c,Rate_j,'b',1:c,Rate_s,'r',...
    1:c,Rate_je,':b',1:c,Rate_se,':r')
ylabel('rate in nats');
xlabel('k-user joint decoder');
title(['Continuous bias distribution / Majority attack / c =',num2str(c)]);
legend('Iterative joint','Iterative single',...
    'Iter. joint erasure','Iter. single erasure',...
    'Location','Best')


