%% Generate pirated sequences according to some models
% and run the estimator for different collusion size and plot likelihood  

fprintf('Test of the collusion model estimation\n')
fprintf('\t generate pirated sequences according to some model\n')
fprintf('\t estimate with different collusion sizes\n\n')

%% Setup
c = 5;
m = 800;
cmax = 10;
lik = zeros(cmax,1);
figure;

fprintf('The true collusion size is %d \n',c)

%% sim 1
% Uniform model
p = generate_secret(m);
X = generate_sequence(p,c);
model = collusion_model_I(c,'uniform');
y = double(collude(X,'strategy','Random','model',model));

for c_e=2:cmax
    [model_e,lik(c_e)]=estimate_model_hard_EM(c_e,y,p,true);
end

subplot(5,1,1)
plot(2:cmax,lik(2:cmax),c,lik(c),'or')
title('Uniform')
ylabel('Likelihood')
xlabel('c_e')


%% sim 2
% Coin Flip model
p = generate_secret(m);
X = generate_sequence(p,c);
model = collusion_model_I(c,'coinflip');
y = double(collude(X,'strategy','Random','model',model));

for c_e=2:cmax
    [model_e,lik(c_e)]=estimate_model_hard_EM(c_e,y,p,true);
end

subplot(5,1,2)
plot(2:cmax,lik(2:cmax),c,lik(c),'or')
title('Coin Flip')
ylabel('Likelihood')
xlabel('c_e')

%% sim 3
% Minority model with erasure
p = generate_secret(m);
X = generate_sequence(p,c);
par.erasure_rate = 0.2;
model = collusion_model_I(c,'minority',par);
y = double(collude(X,'strategy','Random','model',model));

for c_e=2:cmax
    [model_e,lik(c_e)]=estimate_model_hard_EM(c_e,y,p,true);
end

subplot(5,1,3)
plot(2:cmax,lik(2:cmax),c,lik(c),'or')
title('Minority with erasures')
ylabel('Likelihood')
xlabel('c_e')

%% sim 4
% Mixing block with double detections and erasures
p = generate_secret(m);
X = generate_sequence(p,c);
par.double_rate = 0.25;
par.erasure_rate = [0.01 0.4];
par.ratio_deviation = 0.3;
model = collusion_model_II(c,'mixall',par);

y = double(collude(X,'strategy','Random','model',model));

for c_e=2:cmax
    [model_e,lik(c_e)]=estimate_model_hard_EM(c_e,y,p,true);
end

subplot(5,1,4)
plot(2:cmax,lik(2:cmax),c,lik(c),'or')
title('Mixing block with double and erasures')
ylabel('Likelihood')
xlabel('c_e')

%% sim 5
% Uniform model with erasure
p = generate_secret(m);
X = generate_sequence(p,c);
par.erasure_rate = 0.05;
model = collusion_model_I(c,'uniform');
y = double(collude(X,'strategy','Random','model',model));

for c_e=2:cmax
    [model_e,lik(c_e)]=estimate_model_hard_EM(c_e,y,p,true);
end

subplot(5,1,5)
plot(2:cmax,lik(2:cmax),c,lik(c),'or')
title('Uniform with erasures')
ylabel('Likelihood')
xlabel('c_e')


