%% Test_basic.m
% Here is a typical simulation

%% Setup

m = 1024; % Code length
c = 5; % Number of colluders
n = 10^6; % Number of users
cmax = 5; % Parameter for the decoder

fprintf('Here is a typical simulation\n');
fprintf('\t %d users\n',n);
fprintf('\t %d colluders\n',c);
fprintf('\t %d bits\n\n',m);

%% Generate the code
p = generate_secret(m);
tic;X = generate_sequence(p,n);
fprintf('1.  Generation of the code: %f s. \n',toc);

%% Generate the collusion
disp('2.  Collusion')

% the collusion model is
% the interleaving attack (or uniform attack)
% plus 33% erasures
opt_collusion.erasure_rate = 1/3;
theta_true=collusion_model_I(c,'uniform',opt_collusion);

y = collude(X(:,1:c),'strategy','Random','model',theta_true);

%% Estimate the collusion
% assuming cmax colluders
tic;theta = estimate_model_hard_EM(cmax,y,p,true);
fprintf('3.  Estimation of the collusion model: %f s. \n',toc);

%% Compute scores
opt_decoder.type = 'ML'; % Maximum Likelihood decoder
opt_decoder.cmax = cmax;
opt_decoder.theta = theta;
opt_decoder = get_opt_single_decoder(y,p,opt_decoder);

tic; S = accusation_score(X,opt_decoder);
fprintf('4.  Computation fo the scores: %f s. \n',toc);

%% Display scores
[Ssort, Ind] = sort(S,'descend');
fprintf('\n\n Here are the top scores\n')

for j=1:10
   if Ind(j)<=c
    fprintf('%f\t col\n',Ssort(j));
   else
       fprintf('%f\t inn\n',Ssort(j));
   end
end
fprintf('\n\n');

%% Plot
hist(S,40);
hold on
plot(S(1:c),0,'*r')
title('Histogram of the scores')


