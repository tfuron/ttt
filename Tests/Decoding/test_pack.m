%% Test of the packing option for single decoders 

%% Setup
m = 512;
c = 5;
n = 10^5;
pack = true;

fprintf('Test of the packing option\n\t the code is more compact\n\t the computation of scores is faster\n\n')

%% Generate code
p = generate_secret(m);
X = generate_sequence(p,n,pack);
Xc = code_unpack(X(:,1:c));

%% Parameters for the attack
par.erasure_rate = 0.1;
model = collusion_model_I(c,'minority',par);

%% Collusion
y = collude(Xc,'strategy','random','model',model);

%% Decoding with a packed code
% Parameters of the decoder
opt.cmax = c;
opt.type = 'ML';
opt.theta = estimate_model_hard_EM(c,y,p,true); % Estimation of the collusion model
opt.pack = true;
opt = get_opt_single_decoder(y,p,opt);

% Compute scores
tic
s = accusation_score(X,opt);
fprintf('Decoding with a packed code: %f\n',toc);


%% Decoding with an unpacked code
% Parameters of the decoder
opt.pack = false;
opt = get_opt_single_decoder(y,p,opt);
Xu = code_unpack(X);

% Compute scores
tic
su = accusation_score(Xu,opt);
fprintf('Decoding with an non-packed code: %f\n\n',toc);

%% Check
if norm(su-s)<10^-5
    disp('Test OK')
else
    error('Test failed: the pack option is not working.')
end

