%% Comparison of different single decoders
% Maximum Likelihood (eg. Neyman Pearson), Tardos \ Skoric, Oosterwijk and
% Laarhoven
%% Setup
clear;
m = 2*512;
c = 5;
n = 10^5;
decoders = {'ML','Tardos','Oosterwijk','Laarhoven','ML_WCA',...
'compound_WCA','Desoubeaux'};



fprintf('Test comparing 4 single decoders\n')
fprintf('\t Maximum Likelihood (or Neyman-Pearson)\n')
fprintf('\t Tardos / Skoric \n')
fprintf('\t Oosterwijk \n')
fprintf('\t Laarhoven\n')
fprintf('\t Maximum Likelihood for Worst Case Attack\n')
fprintf('\t Generalized Linear WCA')
fprintf('\t Desoubeaux\n\n')


%% Generate code
p = generate_secret(m);
X = generate_sequence(p,n);
Xc = X(:,1:c);

%% Parameters for the attack
par.double_rate = 0.25;
par.erasure_rate = [0.01 0.4];
par.ratio_deviation = 0.3;
model = collusion_model_II(c,'mixall',par);

%% Collusion
y = collude(Xc,'strategy','random','model',model);

%% Estimation of the collusion model
theta = estimate_model_hard_EM(c,y,p,false);

% Parameters of the decoder
opt.cmax = c;
opt.theta = theta;
opt.n = n;

%% Comparison over some decoders
for dec = 1:length(decoders)
    opt.type = decoders{dec};
    opt = get_opt_single_decoder(y,p,opt);
    
    % Compute scores
    s = accusation_score(X,opt);

    %% plots
    figure(1);
    subplot(length(decoders),1,dec);
    hist(s,40); % Histogram of the scores
    hold on;
    plot(s(1:c),zeros(c,1),'r*'); % where are the colluders scores
    hold off;
    title(opt.type);
end

