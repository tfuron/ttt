%% Test of the ML single decoder with doubles and erasures

%% Setup
m = 512;
c = 6;
n = 10^6;

fprintf('The setup is as follows\n');
fprintf('\t %d users\n',n);
fprintf('\t %d colluders\n',c);
fprintf('\t %d bits\n\n',m);

%% Generate code
p = generate_secret(m);
X = generate_sequence(p,n);
Xc = X(:,1:c);
coll = [];

%% Parameters for the attack
par.double_rate = 0.25;
par.erasure_rate = [0.01 0.4];
par.ratio_deviation = 0.3;
model = collusion_model_II(c,'mixall',par);

%% Collusion
y = collude(Xc,'strategy','random','model',model);

%% Iterative single decoder

fprintf('*** 1st simulation ***\n')
fprintf('\t we compute new scores at each iteration\n')
fprintf('\t taking into account the codewords of previously accused colluders\n\n')

% Parameters of the decoder
opt.cmax = c;
opt.type = 'ML';

% Estimation of the collusion model
theta = estimate_model_hard_EM(c,y,p,false);
opt.theta = theta;

Iteration = 1;
opt = get_opt_single_decoder(y,p,opt);
s = accusation_score(X,opt);

figure(1)
subplot(c,1,1);
hist(s(c+1:end),40); hold on
plot(s(1:c),zeros(c,1),'+r');
V = axis;
V(1) = V(1)-10; V(2) = V(2)+20;
axis(V);
title(['Iteration #',num2str(Iteration)]);
hold off;
Ind_rem_coll = true(c,1); % indicator of the remaining colluders

while(nnz(Ind_rem_coll)>2)
    [~,u] = max(s);
    if u<=c % we are good
        Ind_rem_coll(u) = false; 
        opt.coll = Xc(:,~Ind_rem_coll);%take into account the codewords
        % of previously accused colluders
        
        Iteration = Iteration + 1;
        opt = get_opt_single_decoder(y,p,opt);
        s = accusation_score(X,opt);
        
        s_col(~Ind_rem_coll) = NaN; % a trick not to spoil the histogram
        
        subplot(c,1,Iteration);
        hist(s(c+1:end),40); hold on; % histogram of the innocent scores
        plot(s(Ind_rem_coll),zeros(nnz(Ind_rem_coll)),'+r'); % plot the colluders score
        axis(V);
        title(['Iteration #',num2str(Iteration)]);
        hold off;
        
    else % we fail
        error('Fail to accuse a colluder');
        break;
    end
end
% Last Iteration
Iteration = Iteration + 1;
[~,u] = max(s);
if u<=c % we are good
    Ind_rem_coll(u) = false;
    opt.coll = Xc(:,~Ind_rem_coll);
    
    
    opt = get_opt_single_decoder(y,p,opt);
    s = accusation_score(X,opt);
    
    s_col(~Ind_rem_coll) = NaN; % a trick not to spoil the histogram
    
    subplot(c,1,Iteration);
    hist(s(c+1:end),40); hold on;
    plot(s(Ind_rem_coll),zeros(nnz(Ind_rem_coll)),'+r');
    title(['Iteration #',num2str(Iteration)]);
    hold off
end

%% Cleaning
clear opt;


%% Iterative single decoder with re-estimation at each step
% Estimation of the collusion model
% theta = estimate_model_hard_EM(c,y,p,false);

fprintf('*** 2nd simulation ***\n')
fprintf('\t 1.  we compute new estimation of the collusion model at each iteration\n')
fprintf('\t taking into account the codewords of previously accused colluders\n')
fprintf('\t 2.  we compute new scores then\n')
fprintf('\t with the new estimated model and taking into account the codewords of previously accused colluders\n\n')


fprintf('Display\n\t The histogram of the scores - red crosses -> colluders\n')


% Parameters of the decoder
opt.cmax = c;
opt.theta = theta;
opt.type = 'ML';


Iteration = 1;
opt = get_opt_single_decoder(y,p,opt);
s = accusation_score(X,opt);

figure(2)
subplot(c,1,1);
hist(s(c+1:end),40); hold on;
plot(s(1:c),zeros(c,1),'+r');
V = axis;
V(1) = V(1)-10; V(2) = V(2)+20;
axis(V);
title(['Iteration #',num2str(Iteration)])
hold off;
Ind_rem_coll = true(c,1); % indicator of the remaining colluders

while(nnz(Ind_rem_coll)>2)
    [~,u] = max(s);
    if u<=c % we are good
        Ind_rem_coll(u) = false;
        opt.coll = Xc(:,~Ind_rem_coll);
        
        Iteration = Iteration + 1;
        
        opt.theta = estimate_model_hard_EM(c,y,p,false,opt.coll);
        
        opt = get_opt_single_decoder(y,p,opt);
        s = accusation_score(X,opt);
        
        s_col(~Ind_rem_coll) = NaN; % a trick not to spoil the histogram
        
        subplot(c,1,Iteration);
        hist(s(c+1:end),40); hold on; % histogram of the innocent scores
        plot(s(Ind_rem_coll),zeros(nnz(Ind_rem_coll)),'+r'); % plot the colluders score
        axis(V);
        title(['Iteration #',num2str(Iteration)]);
        hold off;
        
    else
        error('Fail to accuse a colluder');
        break;
    end
end
% Last Iteration
Iteration = Iteration + 1;
[~,u] = max(s);
if u<=c % we are good
    Ind_rem_coll(u) = false;
    opt.coll = Xc(:,~Ind_rem_coll);
    
    opt.theta = estimate_model_hard_EM(c,y,p,false,opt.coll);
    
    opt = get_opt_single_decoder(y,p,opt);
    s = accusation_score(X,opt);
    
    s_col(~Ind_rem_coll) = NaN; % a trick not to spoil the histogram
    
    subplot(c,1,Iteration);
    hist(s(c+1:end),40); hold on;
    plot(s(Ind_rem_coll),zeros(nnz(Ind_rem_coll)),'+r');
    title(['Iteration #',num2str(Iteration)]);
    hold off;
end

