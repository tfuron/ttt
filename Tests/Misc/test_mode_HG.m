%% test_mode_HG
% Table 1 in "Fast and stable algorithms for computing and sampling from the noncentral Hypergeometric Distribution"
% Liao & Olsen
% The American Statistician, Vol. 55, No. 4

n = [50 500 5000 50 500 5000];
psi = [1.5 1.5 1.5 6 6 6];
n1 = n;
n2 = n;
m1 = n;


eta_truth = [28 275 2753 36 355 3551];
eta = mode_HG(n1,n2,psi,m1);

if sum(abs(eta_truth-eta))>10^-3
   error('The test of the implementation of the Hypergeometric law has FAILED!') 
else
    disp('Test OK')
end