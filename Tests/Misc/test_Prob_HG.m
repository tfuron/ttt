% test_Prob_HG
% Comparison of Binomial and HyperGeometric distribution

%% First setup
p = 0.5;
c = 10;
n_vec = [20 200 2000];


Pbin = zeros(1,c+1); % Binomial Distribution
for s=0:10
    Pbin(s+1) = nchoosek(c,s).*p.^s.*(1-p).^(c-s);
end

P_HG = zeros(length(n_vec),c+1);
for kn=1:length(n_vec)
    P_HG(kn,:) = Prob_HG(n_vec(kn)*p,n_vec(kn)*(1-p),1,c);
end
figure;
subplot(2,1,1);
stem(0:c,Pbin',':*b');
hold on
stem(0:c,P_HG')
title(['Comparison Binomial vs. Noncentral Hypergeometric / p=',num2str(p)])
legend('Binomial',['HG n=',num2str(n_vec(1))],...
    ['HG n=',num2str(n_vec(2))],...
    ['HG n=',num2str(n_vec(3))])
hold off

%% Second setup
p = 0.1;
Pbin = zeros(1,c+1); % Binomial Distribution
for s=0:10
    Pbin(s+1) = nchoosek(c,s).*p.^s.*(1-p).^(c-s);
end

P_HG = zeros(length(n_vec),c+1);
for kn=1:length(n_vec)
    P_HG(kn,:) = Prob_HG(n_vec(kn)*p,n_vec(kn)*(1-p),1,c);
end

subplot(2,1,2);
stem(0:c,Pbin',':*b');
hold on
stem(0:c,P_HG')
title(['Comparison Binomial vs. Noncentral Hypergeometric / p=',num2str(p)])
legend('Binomial',['HG n=',num2str(n_vec(1))],...
    ['HG n=',num2str(n_vec(2))],...
    ['HG n=',num2str(n_vec(3))])
hold off
