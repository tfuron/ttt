% Author: Teddy Furon
%
% Contact: teddy.furon@inria.fr
%
% Copyright INRIA 2014
%
% Licence: 
% This software and its subprograms are governed by the CeCILL license 
% under French law and abiding by the rules of distribution of free software. 
% See http://www.cecill.info/licences.en.html
%
% Except:
% .Prob_HG.m and mode_HG.m (Distribution of a Hypergeometric distribution) from Liao &
% Olsen, The American Statistician, Vol. 55, No. 4

